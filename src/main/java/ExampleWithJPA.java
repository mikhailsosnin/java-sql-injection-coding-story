// Map of valid JPA columns for sorting
final Map<String,SingularAttribute<Account,?>> VALID_JPA_COLUMNS_FOR_ORDER_BY = Stream.of(
        new AbstractMap.SimpleEntry<>(Account_.ACC_NUMBER, Account_.accNumber),
        new AbstractMap.SimpleEntry<>(Account_.BRANCH_ID, Account_.branchId),
        new AbstractMap.SimpleEntry<>(Account_.BALANCE, Account_.balance))
        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

        SingularAttribute<Account,?> orderByAttribute = VALID_JPA_COLUMNS_FOR_ORDER_BY.get(orderBy);
        if (orderByAttribute == null) {
        throw new IllegalArgumentException("Nice try!");
        }

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Account> cq = cb.createQuery(Account.class);
        Root<Account> root = cq.from(Account.class);
        cq.select(root)
        .where(cb.equal(root.get(Account_.customerId), customerId))
        .orderBy(cb.asc(root.get(orderByAttribute)));

        TypedQuery<Account> q = em.createQuery(cq);
// Execute query and return mapped results (omitted)