private static final Set<String> VALID_COLUMNS_FOR_ORDER_BY
        = Collections.unmodifiableSet(Stream
        .of("acc_number","branch_id","balance")
        .collect(Collectors.toCollection(HashSet::new)));

public List<AccountDTO> safeFindAccountsByCustomerId(
        String customerId,
        String orderBy) throws Exception {
        String sql = "select "
        + "customer_id,acc_number,branch_id,balance from Accounts"
        + "where customer_id = ? ";
        if (VALID_COLUMNS_FOR_ORDER_BY.contains(orderBy)) {
        sql = sql + " order by " + orderBy;
        } else {
        throw new IllegalArgumentException("Nice try!");
        }
        Connection c = dataSource.getConnection();
        PreparedStatement p = c.prepareStatement(sql);
        p.setString(1,customerId);
        // ... result set processing omitted
        }